# Basic Electronics for Embedded systems

## Sensors and Actuators

### Sensors

A sensor monitors environmental conditions such as fluid levels, temperatures, vibrations, or voltage. When these environmental conditions change, they send an electrical signal to the sensor.

![Sensors](https://th.bing.com/th/id/OIP.abOFWXGr3pcjHnl1VWH21AHaGn?w=184&h=180&c=7&o=5&dpr=1.25&pid=1.7)

<b>Types of sensors:</b>

- Temperature sensors
- Vibration sensors
- Security sensors
- Pressure sensors
- Humidity sensors
- Motion sensors
- Light sensors
- Image sensors

### Actuators

An actuator takes an electrical signal and combines it with an energy source to create physical motion.

![Actuators](https://th.bing.com/th/id/OIP.D25weCSpTUVYkItKlEDdFQHaFj?w=226&h=180&c=7&o=5&dpr=1.25&pid=1.7)


<b>Types of actuators:</b>

- Manual actuators
- Pneumatic actuators
- Hydraulic actuators
- Electric actuators
- Spring actuators


### Example

![Workflow](https://www.bing.com/images/search?view=detailV2&ccid=nWUBkGwY&id=640639FC91EAE3D001B0499C5FBC0AD2690ED7C3&thid=OIP.nWUBkGwYHcp0NLcF3vRekQHaCq&mediaurl=https%3a%2f%2fwww.novatec-gmbh.de%2fwp-content%2fuploads%2fsensoractuator-1024x368.png&exph=368&expw=1024&q=Actuator+and+Sensor+Working+Example&simid=608008756127270208&ck=E04B3165748E1061869A80AD1CED5EEC&selectedIndex=13&FORM=IRPRST)



## Analog and Digital signals

Both analog and digital signals are used to transmit data, usually through electric signals.

| Analog | Digital|
|------- |--------|
| An analog signal is a continuous signal that represents physical measurements.| Digital signals are time separated signals which are generated using digital modulation. |
| Denoted by sine waves | Denoted by square waves |
| It uses a continuous range of values that help you to represent information. | Digital signal uses discrete 0 and 1 to represent information. |
| Temperature sensors, FM radio signals, Photocells, Light sensor, Resistive touch screen are examples of Analog signals. | Computers, CDs, DVDs are some examples of Digital signal.|
| The analog signal bandwidth is low. | The digital signal bandwidth is high. |
| It is suited for audio and video transmission. | It is suited for Computing and digital electronics. |
| Analog signal doesn't offer any fixed range. | Digital signal has a finite number, i.e., 0 and 1. |



![Analog and Digital](https://th.bing.com/th/id/OIP.3pbV6kS5b4z7HrxTNcHn0wHaEK?w=313&h=180&c=7&o=5&dpr=1.25&pid=1.7)

## Microcontrollers (vs) Microprocessors

| Microcontrollers | Microprocessors |
|------- |--------|
| Microcontroller is the heart of an embedded system. | Microprocessor is the heart of Computer system. |
| Microcontroller has a processor along with internal memory and I/O components. | It is only a processor, so memory and I/O components need to be connected externally. |
| Microcontrollers Memory and I/O are already present, and the internal circuit is small. | Memory and I/O has to be connected externally, so the circuit becomes large. |
| Cost of the entire system is low. | Cost of the entire system is high. |
| It's simple and inexpensive with less number of instructions to process. | It's complex and expensive, with a large number of instructions to process. |
| It has a CPU along with RAM, ROM, and other peripherals embedded on a single chip. | It has no RAM, ROM, Input-Output units, timers, and other peripherals on the chip. |
| Microcontroller has more register. Hence the programs are easier to write. | Microprocessor has a smaller number of registers, so more operations are memory-based. |
| It is used mainly in a washing machine, MP3 players, and embedded systems. | It is mainly used in personal computers.	|

![mcu vs mp](https://th.bing.com/th/id/OIP.m_NZA97J-6WRyIz2c2PIiwHaEK?w=266&h=180&c=7&o=5&dpr=1.25&pid=1.7)

## Introduction to Raspberry Pi

The Raspberry Pi is a low cost, credit-card sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse. It is a capable little device that enables people of all ages to explore computing, and to learn how to program in languages like Scratch and Python.

It’s capable of doing everything you’d expect a desktop computer to do, from browsing the internet and playing high-definition video, to making spreadsheets, word-processing, and playing games.

![Rpi](https://th.bing.com/th/id/OIP.UBx8rSb9uMmlQPvOlAyMaQHaE9?w=273&h=182&c=7&o=5&dpr=1.25&pid=1.7)

<b>Raspberry Pi Interfaces :</b>

Interfaces are the ways to connect a sensors to a microprocessor. They are,

1. GPIO
2. UART
3. SPI
4. I2C
5. PWM

## Serial and Parallel Communication

| Serial Communication | Parallel Communication |
|------- |--------|
| The process of transmitting data bit by bit in sequence using a single channel. | The process of transmitting data as a whole byte using multiple channels. |
| It uses a single communication link or wire to either transfer or receives data. | It uses multiple communication links or wires to transfer signals. |
| It needs a start and stop bit or an external clock to synchronize the data. | It does not need to synchronize because the whole byte is received in a single clock cycle. |
| It is slower at a short distance and low frequencies. | It is fast at a short distance and low frequency. |
| It is more efficient for long-distance and high frequency. | It is not efficient for long-distance and high frequency. |
| It is design is very simple and cost-effective. | It is expensive having a complex design. |
| The example of serial communications are USB, SATA, I2C, SPI etc. | The example of parallel communications are computer to printer and communication between internal components in embedded systems. |



![Serial vs Parallel](https://th.bing.com/th/id/OIP.dk3N-6qjGNabAoTRKE2xWwHaGm?w=182&h=180&c=7&o=5&dpr=1.25&pid=1.7)

